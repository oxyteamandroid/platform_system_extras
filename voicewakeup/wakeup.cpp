#include <utils/Log.h>
#include <utils/Thread.h>
#include <utils/SystemClock.h>
#include <binder/BinderService.h>
#include "powermanager/IPowerManager.h"
#include "powermanager/PowerManager.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#define FILE_RESOURCE  "/etc/ivModel_v21.irf"
#define FILE_WAKEUP_DEVICE  "/dev/jz-wakeup"
#define FILE_WAKEUP_NODE  "/sys/class/jz-wakeup/jz-wakeup/wakeup"
#define WAKEUP_ENABLE  '1'
#define WAKEUP_DISABLE  '0'

using namespace android;
class WakeUpThread : public Thread {
	class PMDeathRecipient : public IBinder::DeathRecipient {
	friend class WakeUpThread;
	public:
		PMDeathRecipient(const wp<WakeUpThread>& thread) : mThread(thread) {}
		virtual ~PMDeathRecipient() {}
		// IBinder::DeathRecipient
		virtual void binderDied(const wp<IBinder>& who);
	private:
		PMDeathRecipient(const PMDeathRecipient&);
		PMDeathRecipient& operator = (const PMDeathRecipient&);
		wp<WakeUpThread> mThread;
	};

public:
	WakeUpThread();
	virtual ~WakeUpThread();

	void acquireWakeLock();
	void releaseWakeLock();
	void clearPowerManager();

	int setupResource();
	int waitWakeup();
	int enableWakeup();
	int disableWakeup();

private:
	void acquireWakeLock_l();
	void releaseWakeLock_l();
	sp<IPowerManager> mPowerManager;
	sp<IBinder>       mWakeLockToken;
	const sp<PMDeathRecipient> mDeathRecipient;

	int wakeup_fd;

	virtual void onFirstRef()
	{
		run("aaaa", 0);
	}

	virtual  status_t readyToRun()
	{
		return 1;
	}

	virtual bool threadLoop()
	{
		ALOGE("aaaaaaa\n");
		return true;
	}
};

void WakeUpThread::PMDeathRecipient::binderDied(const wp<IBinder>& who)
{
    sp<WakeUpThread> thread = mThread.promote();
    if (thread != 0) {
        thread->clearPowerManager();
    }
    ALOGW("power manager service died !!!");
}

WakeUpThread::WakeUpThread():
	Thread(false),mDeathRecipient(new PMDeathRecipient(this))
{
        sp<IBinder> binder =
		defaultServiceManager()->checkService(String16("power"));
        if (binder == 0) {
		ALOGW("Thread WakeUpThread cannot connect to the power manager service");
        } else {
		mPowerManager = interface_cast<IPowerManager>(binder);
		binder->linkToDeath(mDeathRecipient);
        }
}
WakeUpThread::~WakeUpThread()
{
	releaseWakeLock_l();
	if (mPowerManager != 0) {
		sp<IBinder> binder = mPowerManager->asBinder();
		binder->unlinkToDeath(mDeathRecipient);
	}
}
void WakeUpThread::clearPowerManager()
{
    releaseWakeLock_l();
    mPowerManager.clear();
}

void WakeUpThread::acquireWakeLock_l()
{
	if (mPowerManager != 0) {
		sp<IBinder> binder = new BBinder();
		status_t status =
			mPowerManager->acquireWakeLock(POWERMANAGER_PARTIAL_WAKE_LOCK,
						       binder,String16("bbb"),String16("aaaaa"));//not one way
		if (status == NO_ERROR) {
			mWakeLockToken = binder;
		}
		mPowerManager->wakeUp(uptimeMillis());//not one way
		ALOGV("wakeUp");
		ALOGV("acquireWakeLock_l() status %d", status);
	}
}
void WakeUpThread::releaseWakeLock_l()
{
	if (mWakeLockToken != 0) {
		ALOGV("releaseWakeLock_l()");
		if (mPowerManager != 0) {
			mPowerManager->releaseWakeLock(mWakeLockToken, 0);//not one way
		}
		mWakeLockToken.clear();
	}
}
void WakeUpThread::acquireWakeLock()
{
	acquireWakeLock_l();
}
void WakeUpThread::releaseWakeLock()
{
	releaseWakeLock_l();
}
int WakeUpThread::setupResource()
{
	int fd;
	int dev_fd;
	char buffer[512];

	dev_fd = open(FILE_WAKEUP_DEVICE, O_RDWR);
	if(dev_fd < 0) {
		ALOGE("open wakeup deivce error!!!");
		return -1;
	}
	fd = open(FILE_RESOURCE, O_RDONLY);
	if(fd < 0) {
		ALOGE("open resource file error!!!");
		close(dev_fd);
		return -1;
	}

	while(1) {
		int read_cnt;
		int write_cnt;
		read_cnt = read(fd, buffer, 512);
		write_cnt = write(dev_fd, buffer, read_cnt);
		if(read_cnt == 0) {
			ALOGI("resource file set done.");
			break;
		}
	}

	return 0;
}

int WakeUpThread::enableWakeup()
{
	int fd;
	char temp = '1';
	fd = open(FILE_WAKEUP_NODE, O_RDWR);
	if(fd < 0) {
		ALOGE("open wakeup node for enable failed!!");
		return -1;
	}
	write(fd, &temp, 1);
	close(fd);
	return 0;
}
int WakeUpThread::disableWakeup()
{
	int fd;
	char temp = '0';

	fd = open(FILE_WAKEUP_NODE, O_RDWR);
	if(fd < 0) {
		ALOGE("open wakeup node for disable failed!!");
		return -1;
	}

	write(fd, &temp, 1);
	close(fd);
	return 0;
}

int WakeUpThread::waitWakeup()
{
	char wakeup[32];
	int fd = open(FILE_WAKEUP_NODE, O_RDWR);
	int len;
	if(fd < 0) {
		ALOGE("open wakeup node for waitwakeup failed!");
		return -2;
	}
	len = read(fd, wakeup, 32);
	close(fd);
	if(len < 0) {
		return -1;
	} else {
		return 0;
	}

}

int main(int argc, char *argv[])
{
	int ret;
	WakeUpThread *w;
	w = new WakeUpThread();
	ret = w->setupResource();
	if(ret < 0) {
		goto end;
	}
	ret = w->enableWakeup();
	if(ret < 0) {
		goto end;
	}

	while(1) {
		int n;
		n = w->waitWakeup();
		if(n == -2) {
			ALOGE("voice wakeup error. !!!\n");
			break;
		} else if(n == -1) {
			ALOGE("voice wakeup by signal !!!\n");
		} else if(n == 0) {
			w->acquireWakeLock();
			ALOGI("voice wakeup ok!\n");
			w->releaseWakeLock();
		}
	}

end:
	while(1) {
		sleep(10000);
	}
	return 0;
}
